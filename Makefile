###############################################################################
###          University of Hawaii, College of Engineering
### @brief   Lab 07d - Animal Farm 1 - EE 205 - Spr 2022
###
### @file    Makefile
### @version 1.0 - Initial version
###
###
### @author  Brayden Suzuki <braydens@hawaii.edu>
### @date    22 Feb 2022
###############################################################################

CC     = gcc
CFLAGS = -g -Wall -Wextra

TARGET = animalFarm

all: $(TARGET)

main.o: catDatabase.h addCats.h reportCats.h updateCats.h main.c
	$(CC) $(CFLAGS) -c main.c

catDatabase.o: catDatabase.c catDatabase.h
	$(CC) $(CFLAGS) -c catDatabase.c 

addCats.o: addCats.c addCats.h catDatabase.h
	$(CC) $(CFLAGS) -c addCats.c

reportCats.o: reportCats.c reportCats.h addCats.h catDatabase.h
	$(CC) $(CFLAGS) -c reportCats.c

updateCats.o: updateCats.c updateCats.h addCats.h catDatabase.h
	$(CC) $(CFLAGS) -c updateCats.c

deleteCats.o: deleteCats.c deleteCats.h addCats.h catDatabase.h
	$(CC) $(CFLAGS) -c deleteCats.c

animalFarm: main.o catDatabase.o addCats.o reportCats.o updateCats.o deleteCats.o 
	$(CC) $(CFLAGS) -o $(TARGET) main.o catDatabase.o addCats.o reportCats.o updateCats.o deleteCats.o

clean:
	rm -f $(TARGET) *.o
