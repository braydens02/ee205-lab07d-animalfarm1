/////////////////////////////////////////////////////////////////////////////
//////
/////// University of Hawaii, College of Engineering
/////// @brief Lab 07d - AnimalFarm 0 - EE 205 - Spr 2022
///////
/////// @file catDatabase.h
/////// @version 1.0
///////
/////// @author Brayden Suzuki <braydens@hawaii.edu>
/////// @date 17_Feb_2022
/////////////////////////////////////////////////////////////////////////////////
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_CATS (1024)
#define MAX_CAT_NAMES (50)

enum Gender {UNKNOWN_GENDER, MALE, FEMALE} ;

enum Breed {UNKNOWN_BREED, MAINE_COON, MANX, SHORTHAIR, PERSIAN, SPHYNX} ;

enum Color {UNKNOWN_COLOR, BLACK, WHITE, RED, YELLOW, ORANGE, GREEN, BLUE, PURPLE, PINK} ;

struct Cats{
   char name[MAX_CATS] ;

   enum Gender gender ;

   enum Breed breed ;

   bool isfixed ;

   float weight ;

   enum Color collarColor1 ;

   enum Color collarColor2 ;

   unsigned long long license ;
};

extern unsigned long NUM_CATS ;
extern struct Cats catDatabase[MAX_CATS] ;
extern char* getGender(enum Gender gender) ;
extern char* getBreed(enum Breed breed) ;
extern char* getCollarColor(enum Color color) ;
