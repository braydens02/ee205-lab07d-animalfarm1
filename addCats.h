/////////////////////////////////////////////////////////////////////////////
//////
/////// University of Hawaii, College of Engineering
/////// @brief Lab 07d - Animal Farm 1 - EE 205 - Spr 2022
///////
/////// @file addCats.h
/////// @version 1.0
///////
/////// @author Brayden Suzuki <braydens@hawaii.edu>
/////// @date 18_Feb_2022
/////////////////////////////////////////////////////////////////////////////////
#include <stdbool.h>

extern int addCat(char name[], enum Gender gender, enum Breed breed, bool isfixed, float weight, enum Color collarColor1, enum Color collarColor2, unsigned long long license ) ;
